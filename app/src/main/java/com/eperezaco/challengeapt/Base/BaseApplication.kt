package com.eperezaco.challengeapt.Base

import android.app.Application
import android.content.Context
import com.eperezaco.core.DBRoom.Database.BDRoom
import com.eperezaco.core.WebServices.Controllers.ControllerBank
import com.facebook.stetho.Stetho

class BaseApplication : Application() {

    var dbRoom: BDRoom? = null
    val TAG: String = BaseApplication::class.java.simpleName
    private lateinit var context: Context

    lateinit var controllerBank: ControllerBank

    override fun onCreate() {
        super.onCreate()
        initApplication()
    }

    companion object {
        var instance: BaseApplication? = null
        fun getIntance(): BaseApplication? {
            if (instance == null) {
                instance = BaseApplication()
                instance!!.initApplication()
            }
            return instance
        }
    }

    private fun initApplication() {
        instance = this
        context = applicationContext
        controllerBank = ControllerBank(context)
        Stetho.initializeWithDefaults(this)
        dbRoom = BDRoom.getBDRoom(context)
    }
}