package com.eperezaco.challengeapt

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import com.eperezaco.challengeapt.UI.Activities.Base.BaseAppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class DetailActivity : BaseAppCompatActivity() {

    lateinit var fab: FloatingActionButton
    lateinit var clMain: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initView()
    }

    private fun initView() {
        fab = findViewById(R.id.fab)
        clMain = findViewById(R.id.clMain)
        fab.setOnClickListener { Snackbar.make(clMain, "Welcome", Snackbar.LENGTH_SHORT).show() }
    }
}
