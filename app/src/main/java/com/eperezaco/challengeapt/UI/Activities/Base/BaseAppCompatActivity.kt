package com.eperezaco.challengeapt.UI.Activities.Base

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.eperezaco.challengeapt.R
import com.eperezaco.core.UI.BaseCoreActivity
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.google.android.material.navigation.NavigationView


open class BaseAppCompatActivity : BaseCoreActivity(), DrawerLayout.DrawerListener {

    private lateinit var navigationView: NavigationView
    private lateinit var toggle: ActionBarDrawerToggle
    private var toolbar: Toolbar? = null
    private lateinit var tvTitulo: AppCompatTextView
    private lateinit var ivLogo: AppCompatImageView
    private lateinit var drawer: DrawerLayout
    private lateinit var bottomNavigation: MeowBottomNavigation
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    fun setBottomNavigation() {
        bottomNavigation = findViewById(R.id.bottomNavigationView)

        bottomNavigation.add(MeowBottomNavigation.Model(1, com.eperezaco.core.R.drawable.ic_camera))
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                2,
                com.eperezaco.core.R.drawable.ic_gallery
            )
        )
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                3,
                com.eperezaco.core.R.drawable.ic_settings
            )
        )
        bottomNavigation.add(MeowBottomNavigation.Model(4, com.eperezaco.core.R.drawable.ic_camera))
    }

    var currentTitle = ""
    fun setToolBar(title: String) {
        currentTitle = title
        if (toolbar == null) {
            toolbar = findViewById(R.id.toolbar)
            setSupportActionBar(toolbar)
            if (supportActionBar != null) { //  getSupportActionBar().setHomeAsUpIndicator(R.drawable.flecha_cabeza);
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.icon_challenge)
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                supportActionBar!!.setDisplayShowHomeEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                tvTitulo = findViewById(R.id.titulo)
                ivLogo = findViewById(R.id.ivLogo)
                if (tvTitulo != null) {
                    tvTitulo.visibility = View.VISIBLE
                    tvTitulo.text = title
                    ivLogo.visibility = View.GONE
                } else {
                    tvTitulo.visibility = View.GONE
                    ivLogo.visibility = View.VISIBLE
                }
            }
        } else {
            tvTitulo = findViewById(R.id.titulo)
            ivLogo = findViewById(R.id.ivLogo)
            //    ImageView ivLogoSocio = (ImageView) findViewById(R.id.ivLogo);
            if (tvTitulo != null) {
                tvTitulo.visibility = View.VISIBLE
                tvTitulo.text = title
                ivLogo.visibility = View.GONE
            } else {
                tvTitulo.visibility = View.GONE
                ivLogo.visibility = View.VISIBLE
            }
        }
    }

    override fun onPostCreate(@Nullable savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        this.drawer = findViewById(R.id.navigation_drawer_layout)
        if (drawer != null) {
            //val toolbar: Toolbar = setToolBar(currentTitle, true)
            toggle = ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
            )
            drawer.setDrawerListener(toggle)
            toggle.syncState()
            navigationView = findViewById(R.id.navigation_view)
            if (navigationView != null) {
                setupNavigationDrawerContent(navigationView)
            }

            setupNavigationDrawerContent(navigationView)
            drawer.addDrawerListener(this)
            if (supportActionBar != null) {
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            }
        }
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {

    }

    override fun onDrawerOpened(drawerView: View) {

    }

    open fun setupNavigationDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener(
            object : NavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                    when (menuItem.itemId) {
                        R.id.item_navigation_drawer_inbox -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                        R.id.item_navigation_drawer_starred -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                        R.id.item_navigation_drawer_sent_mail -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                        R.id.item_navigation_drawer_drafts -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                        R.id.item_navigation_drawer_settings -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                        R.id.item_navigation_drawer_help_and_feedback -> {
                            menuItem.isChecked = true

                            drawer.closeDrawer(GravityCompat.START)
                            return true
                        }
                    }
                    return true
                }
            })
    }
}