package com.eperezaco.challengeapt.UI.Activities.Inicio

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eperezaco.challengeapt.DetailActivity
import com.eperezaco.challengeapt.R
import com.eperezaco.challengeapt.UI.Activities.Base.BaseAppCompatActivity
import com.eperezaco.challengeapt.UI.Activities.Inicio.Presenter.BankPresenter
import com.eperezaco.challengeapt.UI.Activities.Inicio.Presenter.BankPresenterImpl
import com.eperezaco.challengeapt.UI.Adapters.AdapterBanks
import com.eperezaco.core.DBRoom.Entity.Bank
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.google.android.material.snackbar.Snackbar
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter

class MainActivity : BaseAppCompatActivity(), BankPresenterImpl.viewBank {

    private lateinit var rvbanks: RecyclerView
    private lateinit var pbUploading: ProgressBar
    private lateinit var clMain: ConstraintLayout
    private lateinit var bottomNavigation: MeowBottomNavigation
    lateinit var presenter: BankPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setToolBar("Main")
        setBottomNavigation()
        initView()
        setPresenter()

    }

    private fun initView() {
        clMain = findViewById(R.id.clMain)
        rvbanks = findViewById(R.id.rvbanks)
        pbUploading = findViewById(R.id.pbUploading)

    }

    private fun setPresenter() {
        pbUploading.visibility = View.VISIBLE
        presenter = BankPresenterImpl()
        presenter.setView(this)
        presenter.getBanks(applicationContext)
    }

    override fun setData(lstLocal: ArrayList<Bank>) {
        pbUploading.visibility = View.GONE
        var adapter = AdapterBanks(applicationContext, lstLocal)
        rvbanks.layoutManager = LinearLayoutManager(applicationContext)
        val alphaAdpt = AlphaInAnimationAdapter(adapter)
        alphaAdpt.setDuration(800)
        val animation =
            AnimationUtils.loadLayoutAnimation(
                applicationContext,
                R.anim.layout_animation_fall_down
            )
        adapter.listener= object :AdapterBanks.listenerBank{
            override fun selectBank() {
                startActivity(Intent(applicationContext, DetailActivity::class.java))
            }

        }
        rvbanks.layoutAnimation = animation
        rvbanks.adapter = alphaAdpt
        rvbanks.scheduleLayoutAnimation()
    }

    override fun setError(message: String) {
        pbUploading.visibility = View.GONE
        Snackbar.make(clMain, message, Snackbar.LENGTH_LONG).show()
    }


}
