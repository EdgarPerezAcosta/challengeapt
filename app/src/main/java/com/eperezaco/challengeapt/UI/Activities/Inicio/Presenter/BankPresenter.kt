package com.eperezaco.challengeapt.UI.Activities.Inicio.Presenter

import android.content.Context

interface BankPresenter {
    fun setView(view: BankPresenterImpl.viewBank)
    fun getBanks(context: Context)
}