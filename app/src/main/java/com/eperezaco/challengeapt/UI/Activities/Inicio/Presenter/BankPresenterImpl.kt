package com.eperezaco.challengeapt.UI.Activities.Inicio.Presenter

import android.content.Context
import com.eperezaco.challengeapt.Base.BaseApplication
import com.eperezaco.challengeapt.R
import com.eperezaco.core.DBRoom.Entity.Bank
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BankPresenterImpl : BankPresenter {

    private lateinit var view: viewBank

    interface viewBank {
        fun setData(lstLocal: ArrayList<Bank>)
        fun setError(message: String)
    }

    override fun setView(view: viewBank) {
        this.view = view
    }

    override fun getBanks(context: Context) {
        var lstLocal = BaseApplication.getIntance()!!.dbRoom!!.bankDao().getAllBank()

        if (lstLocal.isNotEmpty()) {
            view.setData(lstLocal as ArrayList)
        } else {
            BaseApplication.getIntance()!!.controllerBank.getBankList()
                .enqueue(object : Callback<List<Bank>> {
                    override fun onFailure(call: Call<List<Bank>>, t: Throwable) {
                        view.setError(context.resources.getString(R.string.server_error))
                    }

                    override fun onResponse(
                        call: Call<List<Bank>>,
                        response: Response<List<Bank>>
                    ) {
                        var lstBanks = response.body()!! as ArrayList<Bank>
                        BaseApplication.getIntance()!!.dbRoom!!.bankDao().insertAllBank(lstBanks)
                        view.setData(lstBanks)
                    }
                })

        }
    }
}