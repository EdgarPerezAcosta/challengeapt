package com.eperezaco.challengeapt.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eperezaco.challengeapt.R
import com.eperezaco.core.DBRoom.Entity.Bank
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class AdapterBanks(
    private val context: Context,
    private var list: ArrayList<Bank>
) : RecyclerView.Adapter<AdapterBanks.ViewHolder>() {
    lateinit var listener: listenerBank
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bank, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = list[position]
        Picasso.with(context).load(item.url).into(holder.civbank)
        holder.tvTitle.text = item.bankName
        holder.tvAge.text = item.age.toString()
        holder.tvDescription.text = item.description

        holder.itemView.setOnClickListener {
            listener.selectBank()
        }

    }

    override fun getItemId(position: Int): Long {
        return 0
    }


    public class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvAge: TextView = view.findViewById(R.id.tvAge)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var civbank: CircleImageView = view.findViewById(R.id.civbank)
    }

    interface listenerBank {
        fun selectBank()
    }

    fun setOnItemClickListener(listener: listenerBank) {
        this.listener = listener
    }
}