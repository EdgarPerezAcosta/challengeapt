package com.eperezaco.core.DBRoom.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.eperezaco.core.DBRoom.Entity.Bank

@Dao
interface BanksListDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBank(vararg data: Bank)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllBank(lstBank: List<Bank>)

    @Query("DELETE  FROM bank")
    fun clearTableBank()

    @Query("SELECT * FROM bank")
    fun getAllBank(): List<Bank>

}