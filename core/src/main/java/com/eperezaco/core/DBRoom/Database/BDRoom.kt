package com.eperezaco.core.DBRoom.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import com.eperezaco.core.DBRoom.Dao.BanksListDao
import com.eperezaco.core.DBRoom.Entity.Bank


@Database(
    entities = [Bank::class],
    version = 1,
    exportSchema = false
)
abstract class BDRoom : RoomDatabase() {


  public  abstract fun bankDao(): BanksListDao

//    fun migrationSQL(database: SupportSQLiteDatabase) {
//        database.execSQL(
//            "CREATE TABLE IF NOT EXISTS GafeteGS " +
//                    "(id INTEGER NOT NULL," +
//                    "BACKGROUND TEXT,ID_EMPRESA TEXT," +
//                    "NUM_EMPLEADO TEXT,IMG_FOTO TEXT," +
//                    "NOMBRE_EMPLEADO TEXT,PUESTO_EMPLEADO TEXT," +
//                    "NIVEL_CERTIFICACION TEXT,VIGENCIA TEXT," +
//                    "URL_BACKGROUND TEXT,URL_FOTO TEXT," +
//                    " PRIMARY KEY (id))"
//        )
//    }

    companion object {
        var INSTANCE: BDRoom? = null
        val DATABASE_NAME = "ChallengeAndroidPT.db"
        fun getBDRoom(context: Context): BDRoom? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    BDRoom::class.java, DATABASE_NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE
        }
    }

    open fun destroyInstance() {
        INSTANCE = null
    }


}
