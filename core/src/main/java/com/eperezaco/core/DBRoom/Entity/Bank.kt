package com.eperezaco.core.DBRoom.Entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "bank")
class Bank : Serializable {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "bankName")
    lateinit var bankName: String

    @ColumnInfo(name = "description")
    lateinit var description: String

    @ColumnInfo(name = "age")
     var age: Int = 0

    @ColumnInfo(name = "url")
    lateinit var url: String
}