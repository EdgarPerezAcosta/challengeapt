package com.eperezaco.core.WebServices.Controllers

import android.content.Context
import com.eperezaco.core.DBRoom.Entity.Bank
import com.eperezaco.core.WebServices.Services.ApiBankService
import retrofit2.Call

class ControllerBank(val ctx: Context) : ControllerBase(ctx), ApiBankService {

    val serviceInterface: ApiBankService = retrofit.create(ApiBankService::class.java)

    override fun getBankList(): Call<List<Bank>> {
        return serviceInterface.getBankList()
    }

}