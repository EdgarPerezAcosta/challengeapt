package com.eperezaco.core.WebServices.Services

import com.eperezaco.core.DBRoom.Entity.Bank
import retrofit2.Call
import retrofit2.http.GET

interface ApiBankService {

    @GET("bins/19e11s")
    fun getBankList(): Call<List<Bank>>

}